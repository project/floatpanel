Installation
============

This module requires the core CKEditor module.

1.Download the required libraries from https://ckeditor.com/cke4/addon/floatpanel -> Version 4.11.4.

2.Place the library in the root libraries folder (/libraries).

3.Enable Floatpanel module.

LIBRARY INSTALLATION (COMPOSER)
-------------------------------
1.Copy the following into your project's composer.json file.

```
"repositories": [
  "ckeditor-plugin/floatpanel": {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/floatpanel",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/floatpanel/releases/floatpanel_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```
2.Ensure you have following mapping inside your composer.json.
```
"extra": {
  "installer-paths": {
    "libraries/{$name}": ["type:drupal-library"]
  }
}
```
3.Run following command to download required library.
```
composer require ckeditor-plugin/floatpanel
```
4.Enable the Floatpanel module.
