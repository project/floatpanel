<?php

namespace Drupal\floatpanel\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "floatpanel" plugin.
 *
 * @CKEditorPlugin(
 *   id = "floatpanel",
 *   label = @Translation("Float Panel"),
 * )
 */
class FloatPanel extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/floatpanel/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

}
